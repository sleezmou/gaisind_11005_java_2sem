package for_06_05;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.function.Predicate;

public class Main {
    //Task 2 method
    public static int[] predicateMethod(int[] array, Predicate<Integer> predicate) {
        int size = 0;
        int[] a = new int[array.length];

        for (int n : array)
            if(predicate.test(n)) {
                a[size] = n;
                size++;
            }

        return Arrays.copyOfRange(a, 0, size);
    }

    public static void main(String[] args) {
        //Task 1
        Comparator<String> comparator = (o1, o2) -> o1.length() - o2.length();
        TreeSet<String> ts = new TreeSet<>(comparator);

        ts.add("12");
        ts.add("123");
        ts.add("1");
        ts.add("123456");

        for (String s : ts) {
            System.out.println(s);
        }

        System.out.println("---------------------------------");

        //Task 2 call
        int[] a1 = { 1, 0, -2, -7, -10, 18, 56, -99, 23, -5, -27 };
        int[] a2 = {1, 2, 3, 4, 5, 6, 7, -10, -20, 30, 31};

        Predicate<Integer> isNegative = (x) -> x < 0;
        Predicate<Integer> isEven = (x) -> x % 2 == 0;

        a1 = predicateMethod(a1, isNegative);
        a2 = predicateMethod(a2, isEven);

        for (int n : a1)
            System.out.print(n + " ");

        System.out.println();

        for (int n : a2)
            System.out.print(n + " ");

        //Task 3 � ������ for 25_03
    }
}
