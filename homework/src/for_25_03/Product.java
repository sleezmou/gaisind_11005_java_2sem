
package for_25_03;

import java.io.Serializable;

public class Product implements Serializable {
    public String name;
    public int amount;

    public Product(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return name + " " + amount + "\n";
    }
}
