package for_25_03;


import java.io.*;
import java.util.Scanner;

public class Main {
    private static Product[] products = new Product[20];


    public static void main(String[] args) {
        products = readProducts();
        printAll();

        addProduct();
        removeProduct();
        changeAmountOfProduct();
    }

    public static void addProduct() {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter product name and amount. Enter 'exit' if you want to stop adding products");
        String name = in.nextLine();
        if (name.toLowerCase().equals("exit")) {
            printAll();
            return;
        }
        int amount = in.nextInt();

        for (int i = 0; i < products.length; i++) {
            if(products[i] != null && products[i].name.toLowerCase().equals(name.toLowerCase())) {
                products[i].amount += amount;
                break;
            }
            if(products[i] == null) {
                products[i] = new Product(name, amount);
                break;
            }
        }

        writeProductsToFile();
        printAll();
        addProduct();
    }

    public static void printAll() {
        for(Product product : products) {
            if(product != null)
                System.out.println(product.toString());
        }
        System.out.println("-------------------------------");
    }

    public static void removeProduct() {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter name of product you want to be removed(exit to leave)");
        String removedName = in.nextLine();
        if(removedName.toLowerCase().equals("exit")) {
            printAll();
            return;
        }

        try {
            for (int i = 0; i < products.length; i++) {
                if (products[i].name.toLowerCase().equals(removedName.toLowerCase())) {
                    if (products[i + 1] != null) {
                        products[i] = products[i + 1];
                        products[i + 1] = null;
                    } else {
                        products[i] = null;
                    }
                    break;
                }
            }
        } catch (NullPointerException nullPointerException) {
            System.out.println("There's no such product");
            return;
        }

        writeProductsToFile();
        printAll();
        removeProduct();
    }

    public static void changeAmountOfProduct() {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter name and new amount of product(exit to leave)");
        String nameToChangeAmount = in.nextLine();
        if(nameToChangeAmount.toLowerCase().equals("exit")) {
            return;
        }
        int amountToChange = in.nextInt();

        try {
            for (Product product : products) {
                if (product.name.toLowerCase().equals(nameToChangeAmount.toLowerCase())) {
                    product.amount = amountToChange;
                    break;
                }
            }
        } catch (NullPointerException nullPointerException) {
            System.out.println("There's no such product");
        }

        writeProductsToFile();
        printAll();
        changeAmountOfProduct();
    }

    public static Product[] readProducts() {
        try (ObjectInputStream oin = new ObjectInputStream(new FileInputStream("productList.txt"))) {
            return (Product[]) oin.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeProductsToFile() {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("productList.txt"))) {
            out.writeObject(products);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}