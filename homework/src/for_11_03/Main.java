package for_11_03;

import java.util.Scanner;
import java.util.Stack;

public class Main {
    static Scanner sc = new Scanner(System.in);

    static boolean tryParseInt(String input) {
        try {
            Integer.parseInt(input);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


//    private static int polishNotation(String str) {
//        Stack<Integer> stack = new Stack<>();
//
//        String[] splitStr = str.split(" ");
//
//        char c;
//
//        for (int i = 0; i < str.length(); i++) {
//            if (tryParseInt(splitStr[i])) {
//                stack.push(Integer.parseInt(splitStr[i]));
//            } else if (splitStr[i].equals('*')) {
//                c = splitStr[i].charAt(0);
//            }
//        }
//    }

    private static boolean bracketsCheck (String str){
            Stack<Character> stack = new Stack<>();

            for (int i = 0; i < str.length(); i++)
                if (str.charAt(i) == '(' || str.charAt(i) == '{' || str.charAt(i) == '[')
                    stack.push(str.charAt(i));

            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == ')' && stack.peek() == '(')
                    stack.pop();
                if (str.charAt(i) == ']' && stack.peek() == '[')
                    stack.pop();
                if (str.charAt(i) == '}' && stack.peek() == '{')
                    stack.pop();
            }

            return stack.empty();
    }

    public static void main (String[] args) {

        String str = "(){}";
        if (bracketsCheck(str)) {
            System.out.println("Line is correct");
        } else {
            System.out.println("Line is incorrect");
        }

        String str2 = "( 1 + 2 ) * 7 / 21 )";
//        polishNotation(str2);
    }
}
