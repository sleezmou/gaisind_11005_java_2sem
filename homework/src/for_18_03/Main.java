
package for_18_03;

import java.util.HashMap;
import java.util.Map;

public class Main {

    //Частотный словарь
    private static void frequencyVocabulary(String str) {
        String[] splitStr = str.split(" ");

        Map<String, Integer> words = new HashMap<>();

        //Task 1
        for (String s : splitStr) {
            if (!words.containsKey(s)) {
                words.put(s, 1);
            } else {
                words.put(s, words.get(s) + 1);
            }
        }

        System.out.println(words);
    }

    //Информация о покупках
    private static void purchaseInfo(String str) {
        Map<String, Map<String, Integer>> purchaseInfo = new HashMap<>();

        String[] lines = str.split("\n");

        for (int i = 0; i < lines.length; i++) {
            String[] splitLine = lines[i].split(" ");

            Map<String, Integer> tempMap = new HashMap<>();
            tempMap.put(splitLine[1], Integer.parseInt(splitLine[2]));

            if(!purchaseInfo.containsKey(splitLine[0])) {
                purchaseInfo.put(splitLine[0], tempMap);
            } else if (!purchaseInfo.get(splitLine[0]).containsKey(splitLine[1])) {
                int t = Integer.parseInt(splitLine[2]);
                purchaseInfo.get(splitLine[0]).put(splitLine[1], t);
            } else {
                int t = Integer.parseInt(splitLine[2]);
                int t2 = purchaseInfo.get(splitLine[0]).get(splitLine[1]);
                purchaseInfo.get(splitLine[0]).put(splitLine[1], t + t2);
            }
        }

        for(String key1 : purchaseInfo.keySet()) {
            System.out.println(key1 + ":");
            for(String key2 : purchaseInfo.get(key1).keySet()) {
                System.out.print("  " + key2 + " - ");
                System.out.println(purchaseInfo.get(key1).get(key2));
            }
        }
    }

    //Цепь Маркова
    private static void additionalTask3(String str) {
        Map<String, Map<String, Integer>> markovChain = new HashMap<>();


        String[] splitLine = str.split(" ");

        for (int i = 0; i < splitLine.length - 1; i++) {
            Map<String, Integer> tempMap = new HashMap<>();
            tempMap.put(splitLine[i + 1], 1);

            if(!markovChain.containsKey(splitLine[i])) {
                markovChain.put(splitLine[i], tempMap);
            } else if(!markovChain.get(splitLine[i]).containsKey(splitLine[i + 1])) {
                markovChain.get(splitLine[i]).put(splitLine[i + 1], 1);
            } else {
                int t = markovChain.get(splitLine[i]).get(splitLine[i + 1]);
                markovChain.get(splitLine[i]).put(splitLine[i + 1], t + 1);
            }
        }

        for(String key1 : markovChain.keySet()) {
            System.out.println(key1 + ":");
            for(String key2 : markovChain.get(key1).keySet()) {
                System.out.print("  " + key2 + " - ");
                System.out.println(markovChain.get(key1).get(key2));
            }
        }
    }

    public static void main(String[] args) {
        frequencyVocabulary("banana apple banana avocado apple banana");

        String str = "Вася ручка 5\n" +
                "Вася карандаш 3\n" +
                "Петя книга 2\n" +
                "Петя книга 3\n" +
                "Вася книга 1\n" +
                "Петя ручка 4";

        System.out.println();

        purchaseInfo(str);

        System.out.println();

        additionalTask3("А И Т Р А Т А И Г И");
    }
}
