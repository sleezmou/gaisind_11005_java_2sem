package for_01_03;

import java.util.*;

public class Stack<T> implements Iterable<T> {


    @Override
    public Iterator<T> iterator() {
        return new StackIterator() {
            Node<T> temp = head.next;

            @Override
            public boolean hasNext() {
                return temp != null;
            }

            @Override
            public T next() {
                T item = temp.item;
                temp = temp.next;
                return item;
            }
        };
    }

    private static class Node<T> {
        T item;
        Node<T> next;

        Node() {
            item = null;
            next = null;
        }

        Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }

        boolean end() {
            return item == null && next == null;
        }
    }

    private Node<T> head = new Node<T>();

    public void push(T item) {
        head = new Node<T>(item, head);
    }

    public T pop() {
        T result = head.item;

        if (!head.end())
            head = head.next;

        return result;
    }

    public T peek() {
        return head.item;
    }

    abstract class StackIterator implements Iterator<T> {

    }
}