package for_01_03;

import java.util.Iterator;
import java.util.ListIterator;

public class LinkedList<T> implements Iterable<T>{

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    public class ListIterator implements Iterator<T> {
        Node<T> temp = head;

        @Override
        public boolean hasNext() {
            return temp != null;
        }

        @Override
        public T next() {
            T item = temp.item;
            temp = temp.next;
            return item;
        }
    }

    private static class Node<T> {
        T item;
        LinkedList.Node<T> next;

        Node() {
            item = null;
            next = null;
        }

        Node(T item, LinkedList.Node<T> next) {
            this.item = item;
            this.next = next;
        }

        boolean end() {
            return item == null && next == null;
        }
    }

    private Node<T> head;
    private Node<T> end;

    void add(T item) {
        Node<T> node = new Node<>();
        node.item = item;

        if(head == null) {
            head = node;
        } else {
            end.next = node;
        }
        end = node;
    }

    T get(int i) {
        int count = 0;

        Node<T> temp = head;

        while (count != i) {
            count++;
            temp = temp.next;
        }
        return temp.item;
    }

    void remove(int i) {
        if(i == 0) {
            head = head.next;
        }

        int count = 0;
        Node<T> temp = head;
        while (count != i - 1) {
            count++;
            temp = temp.next;
        }

        if(i == size() - 1) {
            temp.next = null;
            end = temp;
        } else {
            temp.next = temp.next.next;
        }
    }

    int size() {
        int c = 0;
        Node<T> temp = head;
        while (temp != null) {
            c++;
            temp = temp.next;
        }
        return c;
    }
}
