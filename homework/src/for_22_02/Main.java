package for_22_02;

public class Main {
    public static void main(String[] args) {
        FourTuple <Integer, Integer, Integer, Integer> fourTuple = makeFourValues();
    }

    public static FourTuple<Integer, Integer, Integer, Integer> makeFourValues() {
        return new FourTuple<>(1, 2, 3, 4);
    }

}
