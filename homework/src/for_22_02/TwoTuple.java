package for_22_02;

public class TwoTuple<A, B> extends OneTuple<A> {
    public final B second;

    public TwoTuple(A first, B second) {
        super(first);
        this.second = second;
    }
}
