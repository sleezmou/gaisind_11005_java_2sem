package for_22_02;

import java.util.Iterator;

public class FibonacciRange implements Iterable<Integer> {
    int x1;
    int x2;
    int x3;
    int length;

    FibonacciRange(int length) {
        x2 = 0;
        x3 = 1;
        this.length = length;
    }

    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return length > 0;
            }

            @Override
            public Integer next() {
                length -= 1;
                x1 = x2;
                x2 = x3;
                x3 = x1 + x2;
                return x1;
            }
        };
    }
}
