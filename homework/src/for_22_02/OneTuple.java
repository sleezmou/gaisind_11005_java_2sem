package for_22_02;

public class OneTuple<A> {
    public final A first;

    public OneTuple(A first) {
        this.first = first;
    }
}
