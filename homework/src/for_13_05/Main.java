package for_13_05;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //Task 1
        String[] strings = {"a1", "a111", "gfdgdfgd", "asfgfghf", "aaa4543534", "agdfig8y59734y5834", "gfdg4534534", "2f54rrf5"};

        Arrays.stream(strings)
                .filter(s -> s.charAt(0) == 'a')
                .sorted(((o1, o2) -> o1.length() - o2.length()))
                .forEach(s -> System.out.println(s));

        //Task 2
        
    }
}
