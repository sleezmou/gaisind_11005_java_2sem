public interface ScheduleHelper {
    void addStudent();

    void addPair();

    void deleteStudent();

    void deletePair();

    void printStudents();

    void printPairs();
}
