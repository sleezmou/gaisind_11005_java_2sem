import java.io.Serializable;


public class Pair implements Serializable, Comparable<Pair> {
    String audience;
    String startTime;
    String endTime;
    String group;
    int month;
    int day;
    int week;

    public Pair(String audience, String startTime, String endTime, String group, int day, int month) {
        this.audience = audience;
        this.startTime = startTime;
        this.endTime = endTime;
        this.group = group;
        this.day = day;
        this.month = month;
        this.week = day % 7;
    }

    @Override
    public String toString() {
        return "Дата: " + day + "." + month + "; Аудитория: " + this.audience + "; Время начала пары: " + this.startTime + "; Время конца пары: " + this.endTime + ";Группа, у которой пара: " + this.group;
    }

    @Override
    public int compareTo(Pair pair) {
        if (this.audience.equals(pair.audience) & this.startTime.equals(pair.startTime) & this.endTime.equals(pair.endTime) & this.group.equals(pair.group))
            return 0;
        return 1;
    }
}
