import java.io.Serializable;


public class Student implements Serializable, Comparable<Student> {
    String name;
    String surname;
    String group;

    public Student(String name, String surname, String group) {
        this.name = name;
        this.surname = surname;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Имя студента: " + this.name + "; Фамилия студента : " + this.surname + "; Группа студента: " + this.group;
    }

    @Override
    public int compareTo(Student student) {
        if (this.group.equals(student.group) & this.name.equals(student.name) & this.surname.equals(student.surname))
            return 0;
        return 1;
    }

}
