import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class ScheduleSaver implements ScheduleHelper{
    private ArrayList<Student> students = new ArrayList<>();
    private ArrayList<Pair> pairs = new ArrayList<>();

    private File studentsFile = new File("C:\\Users\\r4smus\\IdeaProjects\\DoborBallovJava\\files\\StudentList.txt");
    private File pairsFile = new File("C:\\Users\\r4smus\\IdeaProjects\\DoborBallovJava\\files\\PairList.txt");

    void menu() {
        System.out.println("\n-----------" +
                "\nКоманды: " +
                "\nПара - добавить пару" +
                "\nСтудент - добавить студента" +
                "\nВсе пары - вывод всех пар" +
                "\nВсе студенты - вывод всех студентов" +
                "\nУдалить пару - удалить пару" +
                "\nУдалить студента - удалить студента" +
                "\nРасписание на неделю "+
                "\nРасписание на день" +
                "\n-----------\n");

        Scanner sc = new Scanner(System.in);
        String command = sc.nextLine().toLowerCase();

        switch(command) {
            case "пара":
                addPair();
            case "студент":
                addStudent();
            case "все пары":
                printPairs();
            case "все студенты":
                printStudents();
            case "удалить студента":
                deleteStudent();
            case "удалить пару":
                deletePair();
            case "расписание на неделю":
                printWeeklyScheduleForStudent();
            case "расписание на день":
                printDailyScheduleForStudent();
            default:
                menu();
        }
    }

    void getStudentsList() {
        try {
            ObjectInputStream studentsInputStream = new ObjectInputStream(new FileInputStream(studentsFile));
            students.addAll((ArrayList<Student>) studentsInputStream.readObject());
            studentsInputStream.close();
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (EOFException eofException) {
            menu();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    void getPairsList() {
        try {
            ObjectInputStream pairsInputStream = new ObjectInputStream(new FileInputStream(pairsFile));
            pairs.addAll((ArrayList<Pair>) pairsInputStream.readObject());
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (EOFException eofException) {
            menu();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    void sendStudentsToFile() {
        try {
            ObjectOutputStream studentsOutputStream = new ObjectOutputStream(new FileOutputStream(studentsFile));
            studentsOutputStream.writeObject(this.students);
            studentsOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendPairsToFile() {
        try {
            ObjectOutputStream pairsOutputStream = new ObjectOutputStream(new FileOutputStream(pairsFile));
            pairsOutputStream.writeObject(this.pairs);
            pairsOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addStudent() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите имя, фамилию и группу студента");
        Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine());

        if (!this.students.contains(student)) {
            this.students.add(student);
            sendStudentsToFile();
        } else {
            System.out.println("Этот студент уже есть в списке");
        }

        menu();
    }

    public void addPair() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите номер аудитории пары, время начала и конца, группу, у которой будет пара, день и месяц");
        Pair pair = new Pair(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextInt(), sc.nextInt());

        if(!this.pairs.contains(pair)) {
            this.pairs.add(pair);
            sendPairsToFile();
        } else {
            System.out.println("Эта пара уже есть в списке");
        }

        menu();
    }

    public void printDailyScheduleForStudent() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите группу студента, нужный день и месяц");

        String group = sc.nextLine();
        int day = sc.nextInt();
        int month = sc.nextInt();

        pairs.stream()
                .filter(pair -> pair.group.equals(group))
                .filter(pair -> pair.day == (day))
                .filter(pair -> pair.month == month)
                .forEach(pair -> System.out.println(pair.toString()));
        menu();
    }

    public void printWeeklyScheduleForStudent() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите группу студента, номер месяца и номер нужной недели");

        String group= sc.nextLine();
        int month = sc.nextInt();
        int week = sc.nextInt();

        pairs.stream()
                .filter(pair -> pair.group.equals(group))
                .filter(pair -> pair.month == month)
                .filter(pair -> pair.week == week)
                .forEach(pair -> System.out.println(pair.toString()));
    }

    @Override
    public void deleteStudent() {
        System.out.println("Введите имя, фамилию и группу студента, которого хотите удалить из списка");

        Scanner sc = new Scanner(System.in);
        Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine());

        for(Student s : this.students) {
            if(s.compareTo(student) == 0) {
                this.students.remove(s);
                break;
            }

            System.out.println("Такого студента нет в списке");
        }

        sendStudentsToFile();

        menu();
    }

    @Override
    public void deletePair() {
        System.out.println("Введите аудиторию, время начала и конца пары, группу, у которо будет пара, которую хотите удалить");

        Scanner sc = new Scanner(System.in);
        Pair pair = new Pair(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextInt(), sc.nextInt());

        for(Pair p : this.pairs) {
            if(p.compareTo(pair) == 0) {
                pairs.remove(p);
                break;
            }
        };

        sendPairsToFile();

        menu();
    }

    @Override
    public void printStudents() {
        for (int i = 0; i < this.students.size(); i++) {
            System.out.println((i + 1) + ". " + this.students.get(i).toString());
        }

        menu();
    }

    @Override
    public void printPairs() {
        for (int i = 0; i < pairs.size(); i++) {
            System.out.println((i + 1) + ". " + pairs.get(i).toString());
        }

        menu();
    }
}
